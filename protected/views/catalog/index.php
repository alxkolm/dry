<h3>Категории</h3>
<?php
$this->widget('zii.widgets.CListView', array(
	'template' => '{items}',
	'dataProvider' => $catalogProvider,
	'itemView' => '_catalog',
));
?>

<?php if ($itemProvider): ?>
	<h3>Позиции категории «<?php echo $catalog->title?>»</h3>
	<?php
		$this->widget('zii.widgets.CListView', array(
			'template' => '{items}',
			'dataProvider' => $itemProvider,
			'itemView' => '_item',
		));
	?>
<?php endif; ?>