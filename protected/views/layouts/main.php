<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />
	<title><?php echo CHtml::encode(Yii::app()->cms->currentPage->title); ?></title>
</head>

<body>

<div class="container">
	<div class="row page-header" id="header">
		<h1>
			<a href="<?php echo $this->createUrl('');?>"> <?php echo CHtml::encode(Yii::app()->name); ?></a>
			<small>demo site</small>
		</h1>
	</div>
	
	<div class="row">
		<div id="mainmenu" class="span3">
			<?php $this->widget('zii.widgets.CMenu',array(
				'items'=>Yii::app()->cms->menu
				)
			); ?>
		</div>
			
		<div class="span9">
			<?php if(isset($this->breadcrumbs)):?>
				<?php $this->widget('zii.widgets.CBreadcrumbs', array(
					'links'=>$this->breadcrumbs,
				)); ?><!-- breadcrumbs -->
			<?php endif?>
			<?php echo $content; ?>
		</div>
	</div>
	
	<div id="footer" class="row footer">
		<div class="span12">
			Copyright &copy; <?php echo date('Y'); ?> Dry pre 1<br/>
			<?php echo Yii::powered(); ?>
		</div>
	</div>
	
</div>

</body>
</html>