<?php
class DForm extends CWidget
{
	public $model;
	public $record;
	
	public function init()
	{
		if ($this->record == NULL) $this->record = $this->model;
	}
	public function run()
	{
		$this->render('form');
	}
}