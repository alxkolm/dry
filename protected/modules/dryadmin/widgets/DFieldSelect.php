<?php
class DFieldSelect extends DField
{
	/**
	 * htmlOptions для селектора
	 * @var array|null
	 */
	public $htmlOptions = null;

	/**
	 * Данные для селектора
	 * @var array|null
	 */
	public $data = null;

	public function init(){
		if (isset($this->field['htmlOptions'])){
			$this->htmlOptions = $this->field['htmlOptions'];
		}

		if (isset($this->field['data'])){
			$this->data = $this->field['data'];
		}
	}

	public function run()
	{
		$this->render('fields/select');
	}
}