<?php $form=$this->beginWidget('bootstrap.widgets.BootActiveForm', array(
		'id'=>'page-PageForm-form',
		'enableAjaxValidation'=>false,
	)); ?>
	<?php echo $form->errorSummary($this->record);?>
	<fieldset>
		<?php foreach ($this->record->fields() as $attribute => $field): ?>
			<?php
				$widgetClass = 'DField'.ucfirst($field['type']);
				$this->widget($widgetClass, array(
					'form' => $form,
					'record' => $this->record,
					'attribute' => $attribute,
					'field' => $field,
				));
			?>
		<?php endforeach;?>
	</fieldset>
	<div class="actions span12">
		<?php echo CHtml::submitButton('Сохранить', array('class' => 'btn primary')); ?>
		<a class="btn" href="<?php echo $this->controller->createUrl('index', $this->controller->actionParams); ?>">Отмена</a>
	</div>
	
<?php $this->endWidget(); ?>

<script type="text/javascript">
$(document).ready(function (){
	$( '.html-editor' ).ckeditor({toolbar: 'Standard', filebrowserBrowseUrl: '<?php echo $this->controller->createUrl('filemanager/index'); ?>'});
});
</script>