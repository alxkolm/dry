<?php

class DryadminModule extends CWebModule
{
	public $defaultController = 'section/index/section/page';
	protected $_assetsUrl;
	
	
	protected $_structure;

	/**
	 * Возвращает путь до опубликованых assets'ах
	 * @return mixed
	 */
	public function getAssetsUrl()
	{
		if ($this->_assetsUrl === null)
            $this->_assetsUrl = Yii::app()->getAssetManager()->publish(
                Yii::getPathOfAlias('dryadmin.assets'));
        return $this->_assetsUrl;
	}
	
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application
		
		// import the module-level models and components
		$this->setImport(array(
			'dryadmin.models.*',
			'dryadmin.models.form.*',
			'dryadmin.components.*',
			'dryadmin.widgets.*',
			'zii.widgets.*',
		));

		Yii::app()->setComponent('bootstrap', Yii::createComponent(array(
			'class' => 'dryadmin.extensions.bootstrap.components.Bootstrap',
		)));
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// Проверка авторизации
			if (Yii::app()->user->isGuest && Yii::app()->controller->id != 'login')
				Yii::app()->user->loginRequired();
			
			return true;
		}
		else
			return false;
	}
	
	public function getStructure()
	{
		return array(
			'page' => array(
				'class' => 'DSection',
				'label' => 'Страницы',
				'model' => 'Page',
				'titleColumn' => 'title',
			),
			'article' => array(
				'class' => 'DSection',
				'label' => 'Статьи',
				'model' => 'Article',
				'titleColumn' => 'title',
			),
			'catalog' => array(
				'class' => 'DSection',
				'label' => 'Каталог',
				'model' => 'Catalog',
				'titleColumn' => 'title',
				'sections' => array(
					'item' => array(
						'class' => 'DSection',
						'masterRelation' => 'items',
						'label' => 'Позиции',
						'model' => 'Item',
						'titleColumn' => 'title',
					),
				),
			),
		);
	}
}
