<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="ru" />

	<?php echo CHtml::cssFile($this->module->assetsUrl.'/jqui/css/Aristo/Aristo.css')?>
	<?php echo CHtml::cssFile($this->module->assetsUrl.'/elfinder/css/elfinder.css')?>

	<?php
		/**
		 * @var CClientScript $cs
		 */
		$cs = Yii::app()->clientScript;
		$cs->registerCoreScript('jquery');
		$cs->registerCoreScript('jquery.ui');

		$cs->registerScriptFile($this->module->assetsUrl.'/jqui/js/i18n/jquery.ui.datepicker-ru.js', CClientScript::POS_BEGIN);
		$cs->registerScriptFile($this->module->assetsUrl.'/ckeditor/ckeditor.js', CClientScript::POS_BEGIN);
		$cs->registerScriptFile($this->module->assetsUrl.'/ckeditor/adapters/jquery.js', CClientScript::POS_BEGIN);
		$cs->registerScriptFile($this->module->assetsUrl.'/elfinder/js/elfinder.min.js', CClientScript::POS_BEGIN);
		$cs->registerScriptFile($this->module->assetsUrl.'/elfinder/js/i18n/elfinder.ru.js', CClientScript::POS_BEGIN);

		$cs->registerScript('datepickerSetup', "$.datepicker.setDefaults($.datepicker.regional['ru']);", CClientScript::POS_BEGIN);
	?>

	<?php Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/iconic/iconic.css'); ?>
	<?php Yii::app()->clientScript->registerCssFile($this->module->assetsUrl.'/admin-style.css'); ?>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
	<?php echo $content; ?>
</body>
</html>