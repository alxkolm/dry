<?php $this->beginContent('/layouts/body'); ?>
<?php
	$this->widget('bootstrap.widgets.BootNavbar', array(
		'fixed' => true,
		'fluid' => true,
		'brand' => Yii::app()->name,
		'brandUrl' => $this->createUrl('section/index', array('section' => 'page')),
		'items' => array(
			array(
				'class' => 'bootstrap.widgets.BootMenu',
				'items' => array(
					array('label' => 'Выход', 'url' => '#'),
				)
			)
		),
	));
?>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span2">
			<?php
				$this->widget('bootstrap.BootMenu', array(
					'type' => 'list',
					'items' => $this->structureMenu,
				));
			?>
		</div>
		<div class="span10">
			<?php $this->widget('bootstrap.widgets.BootBreadcrumbs', array('links' => $this->breadcrumbs, 'homeLink' => FALSE)); ?>
			<?php echo $content; ?>
		</div>
	</div>
</div>
<?php $this->endContent();?>