<?php $this->beginContent('/layouts/body'); ?>
<div class="container">
	<div class="row" style="padding-top:3em;">
		<div class="span6 offset3">
			<?php echo $content; ?>
		</div>
	</div>
</div>
<?php $this->endContent();?>
