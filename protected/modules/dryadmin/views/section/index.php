<?php
/**
 * @var DSectionController $this
 */
?>

<?php
	$this->widget('bootstrap.widgets.BootGridView', array(
		'dataProvider' => $recordsData,
		'template' => '{items}',
		'itemsCssClass'=>'table table-striped',
		'columns' => $this->getGridColumns(),
	));
?>

<?php
	$this->widget('bootstrap.widgets.BootButton', array(
		'fn'    => 'link',
		'label' => 'Добавить',
		'url'   => $this->createUrl('add', $this->actionParams),
	));
?>
	
