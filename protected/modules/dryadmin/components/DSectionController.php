<?php

/**
 * @property DSection $section
 * @property string $model
 * @property array $breadcrumbs
 * @property array $structureMenu
 */
class DSectionController extends CController
{
	
	public $layout = 'twocolumn';
	
	/**
	 * Название ActiveRecord-модели 
	 * @var string
	 */
	protected $_model = NULL;
	
	protected $_breadcrumbs = NULL;

	protected $_structureMenu = NULL;

	/**
	 * Объект раздела админки
	 * @var DSection
	 */
	protected $_section;
	
	/**
	 * Список записей
	 * @param unknown_type $id
	 */
	public function actionIndex($section = NULL, $id = NULL, $master = NULL)
	{
		/**
		 * @var CActiveRecord $model
		 */
		$model = new $this->model();

		if ($model->asa('deleted')){
			// Непоказываем удаленные
			$model->notRemoved();
		}
		
		if ($model->asa('tree')){
			// Показываем только одну ветку
			if ($id !== NULL) {
				$model->childOf( (int) $id );
			} else {
				$model->root();
			}
		}

		if ($model->asa('position')){
			// Сортируем
			$model->sorted();
		}

		if (!empty($master)) {
			$model->dbCriteria->compare($this->section->foreignColumn,$master);
		}


		// DataProvider для GridView
		$recordsData = new CActiveDataProvider(get_class($model), array(
			'criteria' => $model->getDbCriteria(),
		));

		$this->render('index', array(
			'recordsData' => $recordsData,
			'model' => $model,
		));
	}
	
	public function actionUpdate($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		$record = $model->findByPk($id);
		
		if (isset($_POST[$this->model])) {
			$record->attributes = $_POST[$this->model];
			
			if ($record->validate()){
				$record->save();
				
				$params = $this->actionParams;
				unset($params['id']);
				$this->redirect($this->createUrl('index', $params));
				Yii::app()->end();
			}
		}
		
		$this->render('update', array(
			'record' => $record,
			'model' => $model,
			'modelBehaviors' => $modelBehaviors,
		));
	}

	/**
	 * @param string $section ID раздела
	 * @param integer $id ID записи
	 * @param integer $parent ID родителя
	 * @param integer $master ID мастер-модели
	 */
	public function actionAdd($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		/**
		 * @var CActiveRecord $model
		 * @var CActiveRecord $record
		 */
		$model = new $this->model();
		
		$record = $model;
		
		if (isset($_POST[$this->model])) {
			$record->attributes = $_POST[$this->model];
			
			if ($model->asa('tree') && $parent) {
				$record->parent_id = $parent;
			}
			
			if (!empty($master)) {
				$record->{$this->section->foreignColumn} = $master;
			}
			
			if ($record->validate()){
				$record->save();
				$this->redirect($this->createUrl('index', $this->actionParams));
			}
		}
		
		
		
		$this->render('add', array(
			'record' => $record,
			'model' => $model,
		));
	}
	
	public function actionDelete($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		if (isset($modelBehaviors['deleted']) && $id !== NULL){
			$model->findByPk($id)->remove()->save();
		} elseif ($id != NULL){
			$model->deleteByPk($id);
		}
		$this->redirect($this->createUrl('index', $this->actionParams));
	}
	
	public function actionActivate($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		if (isset($modelBehaviors['active']) && $id !== NULL){
			$model->findByPk($id)->activate()->save();
		}
		
		$this->redirect($this->createUrl('index', $this->actionParams));
	}
	
	public function actionDeactivate($section = NULL, $id = NULL, $parent = NULL, $master = NULL)
	{
		$model = new $this->model();
		$modelBehaviors = $model->behaviors();
		
		if (isset($modelBehaviors['active']) && $id !== NULL){
			$model->findByPk($id)->deActivate()->save();
		}
		
		$this->redirect($this->createUrl('index', $this->actionParams));
	}
	
	/**
	 * Возвращает текущую модель
	 */
	public function getModel()
	{
		if ($this->_model == NULL)
		{
				$this->_model = $this->section->model;	
		}
		
		return $this->_model;
	}

	public function getBreadcrumbs()
	{
		if ($this->_breadcrumbs === NULL) {
			$this->_breadcrumbs = $this->section->getBreadcrumbs($this->actionParams);	
		}
		
		return $this->_breadcrumbs;
	}

	/**
	 * Возвращает объект для управления разделом в админке
	 * @return DSection
	 * @throws CException
	 */
	protected  function getSection()
	{
		if ($this->_section === null){
			$sectionId = $this->actionParams['section'];

			if (($pos = strpos($sectionId, '-')) !== false){
				// Если sectionId составной,
				// то берем первую часть строки до знака тире
				$restId = mb_substr($sectionId, $pos + 1);
				$sectionId = mb_substr($sectionId, 0, $pos);
			}

			if (!isset($this->module->structure[$sectionId]))
				throw new CException('В настройках DryadminModule не найден раздел с id '.$sectionId);

			// Настройки объекта для управления разделом админки
			$params = $this->module->structure[$sectionId];
			$params['id'] = $sectionId;
			$params['controller'] = $this;
			// Объект для управления разделом админки
			$section = Yii::createComponent($params);
			$this->_section = (isset($restId) && $restId !== null) ? $section->getSection($restId) : $section;

			// Устанавливаем раздел текущим
			$this->_section->current = true;

			// Указываем ссылку на контроллер
			$this->_section->controller = $this;
		}
		return $this->_section;
	}

	public function getStructureMenu(){
		if ($this->_structureMenu === null){
			$this->_structureMenu = array();
			foreach ($this->module->structure as $sectionId => $section){
				$this->_structureMenu[] = array(
					'label' => $section['label'],
					'url' => $this->createUrl('section/index', array('section' => $sectionId)),
				);
			}
		}

		return $this->_structureMenu;
	}

    /**
     * Настройка колонок для CGridView
     * @return array
     */
	public function getGridColumns()
	{
		/**
		 * @var CActiveRecord $model
		 */
		$model = new $this->model();
		$columns = array();

		// Первый столбец всегда primaryKey
		$columns[] = array(
			'class' => 'DPkColumn',
			'name' => $model->tableSchema->primaryKey,
			'header' => '#'
		);

        // Добавляем столбцы, определенные в методе $model->fields()
		foreach ($model->fields() as $attribute => $field){
			if (in_array('table', $field)){
				$column = array(
					'name' => $attribute,
				);

				if (array_key_exists('column', $field)){
					// Используем указанные настройки колонки
					$column = CMap::mergeArray($column, $field['column']);
				}

				$columns[] = $column;
			}
		}

        // Добавляем столбцы со ссылками на дочерние элементы
        foreach ($this->section->sections as $sectionId => $section){
            $columns[] = array(
	            'class'           => 'DRelationColumn',
	            'labelExpression' => '$this->section->label." (".count($data->{$this->section->masterRelation}).")"',
	            'urlExpression'   => 'Yii::app()->controller->createUrl("index", array("master" => $data->primaryKey, "section" => $this->section->fullId))',
	            'section'         => $section,
            );
        }

		// В конце добавляем столбцы с кнопками
		$columns[] = array(
			'class' => 'bootstrap.widgets.BootButtonColumn',
			'template' => '{update} {delete} {activate} {deactivate}',
			'buttons' => array(
				'update' => array(
					'label' => 'Редактировать',
					'url' => 'Yii::app()->controller->createUrl("update", array_merge(Yii::app()->controller->actionParams, array("id"=>$data->primaryKey)))',
				),
				'delete' => array(
					'label' => 'Удалить',
					'url' => 'Yii::app()->controller->createUrl("delete", array_merge(Yii::app()->controller->actionParams, array("id"=>$data->primaryKey)))',
				),
				'activate' => array(
					'label' => 'Запись неактивна. Нажмите чтобы активировать',
					'url' => 'Yii::app()->controller->createUrl("activate", array_merge(Yii::app()->controller->actionParams, array("id"=>$data->primaryKey)))',
					'icon' => 'off',
					'visible' => '$data->asa("active") !== null and !$data->active'
				),
				'deactivate' => array(
					'label' => 'Запись активна. Нажмите чтобы деактивировать',
					'url' => 'Yii::app()->controller->createUrl("deactivate", array_merge(Yii::app()->controller->actionParams, array("id"=>$data->primaryKey)))',
					'icon' => 'ok',
					'visible' => '$data->asa("active") !== null and $data->active'
				),
			),

		);
		return $columns;
	}
}