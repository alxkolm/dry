<?php
/**
 * Created by JetBrains PhpStorm.
 * User: alx
 * Date: 01.04.12
 * Time: 20:43
 * To change this template use File | Settings | File Templates.
 */
class DPkColumn extends CDataColumn
{
	/**
	 * Renders the data cell content.
	 * This method evaluates {@link value} or {@link name} and renders the result.
	 * @param integer $row the row number (zero-based)
	 * @param mixed $data the data associated with the row
	 */
	protected function renderDataCellContent($row,$data)
	{
		parent::renderDataCellContent($row, $data);

		/**
		 * @var CActiveRecord $data
		 * @var CController $controller
		 */

		// В дополнение рендерим ссылки
		if ($data->asa('tree') !== null){
			$controller = Yii::app()->controller;

			// Ссылка на добавление дочерних элементов
			$addLink = $controller->createUrl('add', array_merge($controller->actionParams, array('parent' => $data->primaryKey)));
			echo ' '.CHtml::link('+', $addLink, array('title' => 'Добавить в ветку'));

			if ($data->children){
				// Ссылка для перехода в ветку
				$branchLink = $controller->createUrl('index', array_merge($controller->actionParams, array('id' => $data->primaryKey)));
				echo ' '.CHtml::link('&rarr;', $branchLink, array('title' => 'Перейти в ветку'));
			}
		}
	}
}
