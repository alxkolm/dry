<?php
/**
 * Компонент отвечающий за поведение раздела админки
 *
 * @property DSection $master
 * @property DSection[] $sections Подразделы
 * @property string $fullId Полный id раздела включая всех предков
 */
class DSection extends CComponent
{
	static $structure;

	/**
	 * @var Controller Контроллер, который создал этот раздел
	 */
	public $controller;

	/**
	 * @var string ID раздела
	 */
	public $id;

	/**
	 * @var string Полный id раздела включая всех предков
	 */
	protected  $_fullId;

	/**
	 * @var string Название пункта меню для данного раздела
	 */
	public $label;

	/**
	 * @var string Атррибут модели, который будет использоваться как заголовок
	 */
	public $titleColumn;

	/**
	 * @var string Имя класса AR-модели, с которой работает данный раздел админки
	 */
	public $model;

	protected $_sections = array();

	/**
	 * @var string Имя реляции через которую модели текущего раздела доступны в родительской ActiveRecord-модели
	 */
	public $masterRelation;

	/**
	 * @var DSection Родительский раздел
	 */
	public $master;

		
	protected $_foreignColumn;
	
	protected $_breadcrumbs;

	/**
	 * @var bool Являеться ли раздел текущим. Т.е. тем с которым идет раобта в админке
	 */
	public $current = false;
	
	public function getForeignColumn()
	{
		if ($this->_foreignColumn === NULL && $this->master != NULL) {
			$masterModel = new $this->master->model();
			$masterRelations = $masterModel->relations();
			$this->_foreignColumn = $masterRelations[$this->masterRelation][2];
		}
		
		return $this->_foreignColumn;
	}

	public function getBreadcrumbs($params)
	{
		if ($this->_breadcrumbs === NULL) {
			$model = new $this->model();

			$this->_breadcrumbs = array();
			
			// master-модель
			if ($this->master && $params['master'])
			{
				$masterParams = array('id' => $params['master'], 'section' => $this->master->id);
				$this->_breadcrumbs += ($this->master->getBreadcrumbs($masterParams));
			}
			
			// Название текущего раздела
			if ($params['master'])
				$this->_breadcrumbs[$this->label] = Yii::app()->controller->createUrl('index', array('section' => $this->fullId, 'master' => $params['master']));
			else
				$this->_breadcrumbs[$this->label] = Yii::app()->controller->createUrl('index', array('section' => $this->fullId));
			
			// Цепочка родителей
			$this->_breadcrumbs += $this->treeBreadcrumbs($params);
			
			// Текущая запись
			if ($this->current) {
				if ($this->controller->action->id == 'add')
					$label = 'Добавить';
				elseif ($this->controller->action->id == 'update')
				{
					if ($params['id']){
						$record = $model->findByPk($params['id']);
						$label = $record->{$this->titleColumn}.' [редактировать]';
					}
				}
			}
			elseif ($params['id']) {
				$record = $model->findByPk($params['id']);
				$label = $record->{$this->titleColumn};
			}

			if ($label && $this->current) $this->_breadcrumbs[$label] = NULL;
			
		}
		
		return $this->_breadcrumbs;
	}
	
	private function treeBreadcrumbs($params)
	{
		$breadcrumbs = array();
		$model = new $this->model();

		if ($model->asa('tree')) {

			if ( isset($params['id'])) {
				$params['parent'] = $model->findByPk($params['id'])->primaryKey;
			}
			
			if (!$params['parent']) return $breadcrumbs;
			
			$parent = $model->findByPk($params['parent']);

			// Если мы в редактировании записи, то прописываем ссылку на первого родителя
			$breadcrumbs[$parent->{$this->titleColumn}] =
				($params['id'])
				? array('index', 'section' => $params['section'], 'id' => $parent->primaryKey)
				: NULL;
			
			// Собираем остальных родителей
			if ($parent->parents)
			{
				foreach ($parent->parents as $p)
				{
					$breadcrumbs[$p->{$this->titleColumn}] = array('index', 'section' => $params['section'], 'id' => $p->primaryKey);
				}
			}
			
		}
		
		return array_reverse($breadcrumbs);
	}

	public function setSections($value)
	{
		// При назначении создаем объекты для разделов
		foreach ($value as $sectionId => $section){
			if (!is_object($section)){
				$value[$sectionId] = Yii::createComponent($section + array('id' => $sectionId, 'master' => $this));
			}
		}

		$this->_sections = $value;
	}

	public function getSections()
	{
		return $this->_sections;
	}

	public function getFullId()
	{
		return ($this->master === null) ? $this->id : $this->master->fullid.'-'.$this->id;
	}

	/**
	 * Возвращает подраздел
	 * @param $sectionId
	 * @return DSection
	 */
	public function getSection($sectionId){
		if (($pos = strpos($sectionId, '-')) !== false){
			$pos = mb_strpos($sectionId, '-');
			$partId = mb_substr($sectionId, 0, $pos);
			$restId = mb_substr($sectionId, $pos + 1);
			return $this->sections[$partId]->getSection($restId);
		} else {
			return $this->sections[$sectionId];
		}
	}

}
