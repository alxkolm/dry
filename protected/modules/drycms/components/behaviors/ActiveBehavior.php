<?php
class ActiveBehavior extends CActiveRecordBehavior
{
	public $field_active = 'active';
	protected $_isActive; 
	
	public function isActive()
	{
		return (boolean) $this->getOwner()->{$this->field_active};
	}
	
	public function getIsActive()
	{
		return $this->isActive();
	}
	
	public function active()
	{
		$this->getOwner()->dbCriteria->compare($this->field_active, 1);
		return $this->getOwner();
	}
	
	public function notActive()
	{
		$this->getOwner()->dbCriteria->compare($this->field_active, 0);
		return $this->getOwner();
	}
	
	public function activate()
	{
		$this->getOwner()->{$this->field_active} = 1;
		return $this->getOwner();
	}
	
	public function deActivate()
	{
		$this->getOwner()->{$this->field_active} = 0;
		return $this->getOwner();
	}
}