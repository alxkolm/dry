<?php

class m111112_143322_alter_users_table extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('users', 'login', 'string NOT NULL');
		$this->alterColumn('users', 'email', 'string NOT NULL');
		$this->createIndex('login', 'users', 'login', true);
	}

	public function down()
	{
		$this->dropIndex('login', 'users');
		$this->alterColumn('users', 'login', 'text');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}