<?php
/**
 * User: Алексей
 * Date: 02.06.12
 * Time: 10:34
 */
class CatalogController extends Controller
{
	public function actionIndex($id = 0){
		$catalog = $id == 0 ? null : $this->loadModel($id, 'Catalog');

		// Используя методы из TreeBehavior и ActiveBehavior выбираем все актинвые подкатегории
		$catalogCriteria = Catalog::model()->childOf($id)->active()->getDbCriteria();

		// dataprovider для подкатегорий
		$catalogProvider = new CActiveDataProvider('Catalog', array(
			'criteria' => $catalogCriteria,
		));

		if ($catalog !== null){
			$itemProvider = new CActiveDataProvider('Item', array(
				'data' => $catalog->items,
			));
		}

		$this->render('index', array(
			'catalogProvider' => $catalogProvider,
			'itemProvider'    => $itemProvider,
			'catalog'         => $catalog,
		));
	}

	public function actionItem($id){
		$item = $this->loadModel($id, 'Item');

		$this->render('item', array(
			'item' => $item,
		));
	}

	public function loadModel($id, $class = 'Catalog'){
		$staticModel = new $class();
		$model = $staticModel->findByPk($id);

		if ($model === null){
			throw new CHttpException('Запрошенная страница не существует');
		}

		return $model;
	}
}
